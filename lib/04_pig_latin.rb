# Pig Latin is a made-up children's language that's intended to be
# confusing. It obeys a few simple rules (below) but when it's spoken
# quickly it's really difficult for non-children (and non-native
# speakers) to understand.
#
# Rule 1: If a word begins with a vowel sound, add an "ay" sound to
# the end of the word.
#
# Rule 2: If a word begins with a consonant sound, move it to the end
# of the word, and then add an "ay" sound to the end of the word.
#
# (There are a few more rules for edge cases, and there are regional
# variants too, but that should be enough to understand the tests.)
#
# See <http://en.wikipedia.org/wiki/Pig_latin> for more details.

require "byebug"

def translate(text)
  arr = text.split(" ").map do |el|
    punctuation(capitals(simple_translation(el)))
  end
  arr.join(" ")
end

# function for the translation of single words
def simple_translation(text)
  # debugger
  vowels = "AEIOUaeiou"
  # if the first letter is a vowel, just add 'ay' and your work is done
  if vowels.include?(text[0])
    text += "ay"
  else
    ind_first_vowel = first_vowel(text)
    "#{text[ind_first_vowel..-1]}#{text[0...ind_first_vowel]}ay"
  end
end

# helper function for finding the index of the first vowel
def first_vowel(text)
  vowels = "aeiou"
  text.each_char.with_index do |el, ind|
    # this is the index of the first vowel
    # it will let me take off the first syllable
    if vowels.include?(el)
      if el == "u" && text[ind - 1] == "q"
        next
      else
        return ind
      end
    end
  end
end

# helper function for finding words with capitals
# if a word has a capital in its middle, it will return it
# with only the first letter capitalized
def capitals(word)
  up_letters = ("A".."Z").to_a
  word.each_char.with_index do |el, ind|
    if ind == 0
      next
    elsif up_letters.include?(el)
      # capitals found
      return word.capitalize
    end
  end
  # no capitals found
  word
end

# finds punctuation in a word and punts it to the back
def punctuation(word)
  marks = ",.?;:!-"
  punctuation_stump = ""
  word.each_char do |el|
    punctuation_stump += el if marks.include?(el)
  end
  "#{word.delete(marks)}#{punctuation_stump}"
end
