def echo (word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num=2)
  reps = "#{word} " * num
  reps[0...-1]
end

def start_of_word(word, lets)
  word[0...lets]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  little_words = ["the", "over", "and"]
  title_arr = str.split(" ").each_with_index.map do |el, ind|
    if ind == 0
      el.capitalize
    elsif little_words.include?(el)
      el
    else
      el.capitalize
    end 
  end
  title_arr.join(" ")
end
